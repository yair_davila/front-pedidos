import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import Tablero from './components/Tablero';

ReactDOM.render(
  <React.StrictMode>
    
    <Tablero/>
  </React.StrictMode>,
  document.getElementById('root')
);
