import React from 'react'

class Tablero  extends React.Component{
    state = {
        pedidos : [],
        isFetch: false
    }
    

    async componentDidMount(){
        await this.fetchPedidos()
    }

    fetchPedidos = async() =>{
        
        let res = await fetch('http://localhost:8000/pedidosDetalle?format=json')
        let data = await res.json()
        this.setState({pedidos:data,isFetch:true})
    }

    

    render(){
        if(this.state.isFetch){
            return (
                <div className="container mt-5">
                    <h1>Seguimiento de Pedidos</h1>
                    <table className="table table-bordered">
                        <thead className="thead-light">
                            <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Fecha pedido</th>
                            <th scope="col">Fecha entrega</th>
                            <th scope="col">Urgente</th>
                            <th scope="col">Cliente</th>
                            <th scope="col">Direccion Cliente</th>
                            <th scope="col">Tipo Cliente</th>
                            <th scope="col">Despacho</th>
                            <th scope="col">Tipo Pedido</th>
                            </tr>
                        </thead>
                    {
                        this.state.pedidos.pedidos.map(item =>(
                            <tr key={item.pedido_id}>
                                <td>{item.pedido_id}</td>
                                <td>{item.fecha_pedido}</td>
                                <td>{item.fecha_entrega}</td>
                                <td>{item.urgente}</td>
                                <td>{item.cliente}</td>
                                <td>{item.direccion_cliente}</td>
                                <td>{item.tipo_cliente}</td>
                                <td>{item.despacho}</td>
                                <td>{item.tipo_pedido}</td>
                            </tr>
                        ))
                    }
                    </table>
                </div>
            )
        }
        return "Cargando"
    }
}

/*
const Tablero = () => {

    const op = async() =>{
        try {
            const res = await fetch('http://192.168.0.129:8000/?format=json')
            console.log(res.statusText)
            console.log(res)
            const data = res.json()
            console.log(data)
            console.log(data.results)
        } catch (error) {
            
        }
    }
    op()

    return (
        
        <div className="container mt-5">
            <h1>Hola mundo</h1>
        </div>
    )
}
*/
export default Tablero
